#!/bin/bash

URL_LATEST=https://gitlab.com/abmyii/sdl/-/jobs/artifacts/ubports/download?job=xenial_${ARCH}_deb

sudo apt-get purge libsdl2-2.0-0:${ARCH} -y

#wget ${URL_LATEST} -O build.zip
#unzip build.zip
sudo dpkg -i --force-overwrite build/libsdl2_*.deb

ls -alh /usr/lib/${ARCH_TRIPLET}/libSDL2-2.0.so*

cp /opt/godot/godot .
